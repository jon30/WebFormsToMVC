﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SimpleLabel.aspx.cs" Inherits="WebFormsToMVC.SimpleLabel" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:Button ID="btnChangeLabel" runat="server"
                OnClick="btnChangeLabel_Click"
                Text="Boring, give me another greeting" />
            <asp:Label ID="lblGreeting" runat="server">Welcome one and all</asp:Label>
        </div>
    </form>
</body>
</html>
