﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebFormsToMVC
{
    public partial class SimpleLabel : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnChangeLabel_Click(object sender, EventArgs e)
        {
            this.lblGreeting.Text = "Hello, this is the changed greeting :-)";
        }
    }
}