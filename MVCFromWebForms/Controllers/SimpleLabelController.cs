﻿using Microsoft.AspNetCore.Mvc;
using MVCFromWebForms.Models;

namespace MVCFromWebForms.Controllers
{
    [Route("~/")] // ensures requests to the default url also end up here (check out startup.cs for another way to do routing)
    [Route("[controller]")] // ensures requests to /SimpleLabel end up here
    public class SimpleLabelController : Controller
    {
        [HttpGet("")] // GET requests to SimpleLabel/
        public IActionResult Get()
        {
            var model = new SimpleLabelModel { Greeting = "Welcome one and all" };
            return View("Index", model);
        }

        [HttpPost("")] // POST requests to SimpleLabel/
        public IActionResult WelcomeMe()
        {
            var model = new SimpleLabelModel
            {
                Greeting = "Hello, this is the changed greeting :-)"
            };
            return View("Index", model);
        }
    }
}